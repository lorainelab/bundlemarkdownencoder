package org.lorainelab.maven.plugins;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 *
 * @author dcnorris
 */
@Mojo(name = "encodeMarkdown", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class BundleMarkdownEncoder extends AbstractMojo {

    @Parameter(property = "markdownFile", required = false, defaultValue = "README.md")
    private File markdownFile;

    @Parameter(readonly = true, defaultValue = "${project}")
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException {
        if (markdownFile.exists()) {
            try {
                getLog().info("Running BundleMarkdownEncoder");
                project.getProperties().put("bundleDescription", Base64.getEncoder().encodeToString(Files.toString(markdownFile, Charsets.UTF_8).getBytes()));
            } catch (IOException ex) {
                getLog().error(ex.getMessage(), ex);
            }
        }
    }

}
